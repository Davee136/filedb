export { FileDB } from './core/index';

// Testing
// import { log as c } from 'console'

// const db = new FileDB({
//   name: 'users',
//   location: 'default',
//   defaults: {
//     post: [],
//     user: [],
//     tasks: [] 
//   }
// })

/* 
  TASK TO DO

  (-) Add
  (-) multiple add
  (-) Get all, single get   
  () Delete all o single element
*/


/* 
  Posible names
  shortdb
  leddb
  slut
  slot
  slow
  slab
  stow
*/


/* -------------------- Add new item | items -------------------- */
// db
// Single
  // .add('post', { id: 1, title: 'Awesome title' })
  // .add('post', { id: 2, title: 'Another title :)' })

// multiples
  // .add('tasks', [
  //   {id: 1, done: true},
  //   {id: 3, done: false},
  //   {id: 4, done: true},
  //   {id: 5, done: false},
  //   {id: 6, done: true},
  //   {id: 1, done: true}
  // ]) 

/* 
  ----- Single
  add(param1, param2)

  param1(string) => Nombre del nuevo objeto
  param2(object) => El objeto a ingresar

  ---- Multiples
  add(param1, [param2])

  param1(string) => Nombre del nuevo objeto
  param2(array)  => Array de nuevos objetos a ingresar

*/

/* -------------------- Get items | single item -------------------- */

// db
  //.get('post') // return all
  // .get('post', { id: 2 }) // return specific

/* 
  ------ Multiples ------
  get(param)

  param => Nombre de objeto a obtener

  ------ Single ------
  get(param1, param2)

  param1(string) => Nombre del objeto
  param2(object) => obj { id: Identificador }

*/

/* -------------------- Update object -------------------- */
// db
// To do best fn: Update
  // .update('post', { id: 1, title: 'Title updated' })

/* 
  update(param1, param2)

  param1 => Nombre del objeto a actualizar
  param2 => Objeto 
  { 
    id: Puede ser cualquiera indicado por el usuario, por defecto sera ID,
    fields: los otros campos que seran actualizados.
  }

*/

/* -------------------- Delete unique or all -------------------- */
// db
//   .delete('user')
/* 
  ---- All
  delete(param1)

  param1(String) => To be removed

  // Specific
  delete(param1, param2)

  param1(String) => Object name
  param2(Object) => {
    id: ID to remove the object
  }


*/

/* -------------------- Delete JSON file -------------------- */
// async function clean() {
//   await db.deleteDB()
//   await db.deleteAll()
// }

// clean()