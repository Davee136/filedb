import Transaction from './transaction'
import { extract, removeFile, removeFolder ,createFile } from './helpers'

/** 
 * Class representing database 
 * @extends Transaction
 */
class FileDB extends Transaction {
  /**
   * Set default configuration
   * @constructor
   * @param {Object} defaults - Default DB configuration
   * @param {string} defaults.name - The DB name
   * @param {string} defaults.location - Custom or default location
   * @param {object} [defaults.defaults={}] - Some data to JSON file
   */
  constructor({name, location, defaults={}}) {
    const local = extract(name, location) 
    
    super(local)
    this.name = name
    this.location = local
    this.defaults = defaults
    this.#init()
  }

  /**
   * Create initials JSON file
   * @private
   */
  #init() {
    createFile(this.location, this.defaults)
  }

  /** 
   * Delete the JSON file database 
   * @returns {Promise}
   */
  async deleteDB() {
    return removeFile(this.location)
  }

  /**
   * Delete the main folder
   * @returns {Promise}
   */
  async deleteAll() {
    return removeFolder()
  }
}

export default FileDB