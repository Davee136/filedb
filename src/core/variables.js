import { homedir } from 'os'

/** Folder when JSON files will be saved */
const FILENAME = '.filedb'

/** Complete folder path */
export const PATH = `${homedir()}/${FILENAME}`