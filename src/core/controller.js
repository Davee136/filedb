import { readJson, readJsonSync, outputJsonSync, writeJsonSync } from 'fs-extra'
import { isArray } from './helpers'

/** All logic of the database */
class Controller {
  /**
   * Set the JSON path
   * @param {string} location - The path of the JSON DB
   */
  constructor(location) {
    this.path = location 
  }

  /* -------------------- Private -------------------- */
  /**
   * Filter and return only the data that's not exist
   * @param {array} data - Data to find
   * @param {array} database - Array of objects
   * @param {string} prop - Property to comparate
   * @returns {array} - New data not found
   * @private
   */
  #_filter(data, database, prop = 'id') {
    let data_filtered = data.filter(item => {
      let res = database.find(dbdata => {
        return dbdata[prop] == item[prop];
      })
      return res == undefined;
    })
    return data_filtered;
  }

  /**
   * Get asynchronously data from JSON DB
   * @returns {Promise}
   */
  async #_data() { return await readJson(this.path) }

  /**
   * Get synchronously JSON data
   * @returns {object}
   */
  #_dataSync() { return readJsonSync(this.path) }

  /**
   * Write the new data to DB
   * @param {string} name - The key 
   * @param {object} obj - New object to add
   * @private
   */
  async #_write(name, obj) {
    // const {  } = Object
    const db = await readJson(this.path)

    // Check if json is empty
    try {

      if (!Object.keys(db).length) {
        db[name] = obj
        return await outputJsonSync(this.path, db)
      } else {
        // Returned not existing files
        const result = Object.keys(db).filter(value => value === name && name)

        // If no exist then push the new data
        if (!result.length) {
          db[name] = obj
          return await outputJsonSync(this.path, db)
        } else {

          // console.log(db)

          if (!isArray(db[name])) {
            const temp = db[name]
            db[name] = [temp] // Add older
            db[name].push(obj) // push new
          } else {
            // Loop and return the new data
            const filteredItems = this.#_filter(isArray(obj)?obj:[obj], db[name])
            filteredItems.length 
              ? filteredItems.map(item => db[name].push(item))
              : false
 
          }

          return await outputJsonSync(this.path, db)
        } 
      }

    } catch (e) {
      console.error('Was an error at: ',e) 
    }
  }

  /**
   * Get the values from DB
   * @param {string} name - The key to get
   * @param {object} [id=false] - The specific ID
   * @returns {array|object}
   * @private
   */
  #_get(name, id=false) {
    const db = readJsonSync(this.path)

    if (!id) {
      return db[name] // return all 
    } else {
      // TODO
      return db[name]
    }
  }

  // #_update(name, value) {}

  /* -------------------- Public -------------------- */
  /**
   * Call the private function to add new data
   * @param {string} name - Key to get value from DB
   * @param {object} obj - New data to write
   */
  write(name, obj) {
    this.#_write(name, obj)
  }

  /**
   * General object from JSON database
   * @param {string} name - Key name to get from DB
   * @returns {array} - All content from database
   */
  find(name) {
    return this.#_get(name)
  }

  /**
   * Returns specific data from database
   * @param {string} name - Key name to get from DB
   * @param {object} id - Specific ID
   */
  findById(name, id) {
    const items = this.#_get(name, id)
    return items.filter(item => item.id === id.id)[0]
  }

  /*
   *  Need to change the mode of the data is saved,
   *  Is necessary, to change the algorithm to loop all data
   *  
   */
  update(data, value) {
    const db = this.#_dataSync()
    
    // Check if id exist
    if (!db[data].find(item => item.id === value.id))
      throw new TypeError(`The data with id ${value.id} isnt exist`)

    const updated = db[data].map(item => (item.id === value.id) ? item = value : item)

    db[data] = updated

    // Write the updated data
    try {
      outputJsonSync(this.path, db)
      return true
    } catch (e) {
      throw new Error(`Was an error to update the files: ${e}`)
    }

  }

  /**
   * Remove general or specific value
   * @param {string} name - Name of the key to delete
   * @param {object} item - To delete specific item
   */
  remove(name, item) {
    const db = this.#_dataSync()
    let newDB = {}

    if (db[name]) {

      if (item) {
      // Delete a single element
        if (!db[name].find(el => el.id === item.id))
          throw new TypeError(`The data with id "${item.id}" not exist`) 

        db[name] = db[name].map(element => {
          if (element.id !== item.id) 
            return element
        }).filter(item => item !== undefined)
      
        newDB = db

      } else {
      // Delete the complete data
        for (const [item, value] of Object.entries(db)) {
          if (item === name) {
            continue
          } else {
            newDB[item] = value
          }
        }
      }

    } else {
      throw new TypeError(`Data with name "${name}" not exist`)
    }

    try {
      outputJsonSync(this.path, newDB)
    } catch (e) {
      throw new Error(`Was an error to delete the element: ${e}`)
    }
  }

}

export default Controller